package com.example.gxh.exception;

/**
 * @version 1.0
 * @Author gxh
 * @Date 2022/7/28 15:43
 */
public class EntityException  extends RuntimeException{


    private String message;



    public EntityException(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
