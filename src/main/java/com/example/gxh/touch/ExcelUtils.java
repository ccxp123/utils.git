package com.example.gxh.touch;


import com.example.gxh.annotations.ExcelToEntityNotNull;
import com.example.gxh.exception.EntityException;
import com.example.gxh.utils.StringUtils;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 *
 * @author gxh
 * @date 2022-07-28 11:42
 * 校验对象属性是否为null
 *
 */
public class ExcelUtils {
   
    /**
     * 判断类属性是否为空
     * @param t
     * @param <T>
     * @throws RuntimeException
     */
    public static <T>  void checkUserFiled(T t){
        if(null == t){
            throw new EntityException("数据为空");
        }
        //获取class对象
        Class<?> aClass = t.getClass();
        //获取当前对象所有属性  使用带Declared的方法可访问private属性
        Field[] declaredFields = aClass.getDeclaredFields();
        //遍历对象属性
        for (Field field : declaredFields) {
            //开启访问权限
            field.setAccessible(true);
            //使用此方法 field.get(Object obj) 可以获取  当前对象这个列的值
            Object o = null;
            try {
                o = field.get(t);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            Annotation annotation = field.getDeclaredAnnotation(ExcelToEntityNotNull.class);
            //如果没有设置当前注解 不用校验
            if(annotation == null){
                continue;
            }
            //获取注解接口对象
            ExcelToEntityNotNull notNull = (ExcelToEntityNotNull)annotation;
            //如果设置了当前注解，但是没有值，抛出异常
            if (o instanceof String){
                o = (String)o;
                if(o == null || "".equals(o) || ((String) o).trim().length() == 0){
                    if(StringUtils.isNotEmpty(notNull.message())){
                        //设置了注解message值 直接返回
                        throw new EntityException(notNull.message());
                    }else{
                        //没有设置可以拼接
                        throw new EntityException(field.getName()+" 不能为空");
                    }
                }
            }else {
                if(o == null){
                    if(StringUtils.isNotEmpty(notNull.message())){
                        //设置了注解message值 直接返回
                        throw new EntityException(notNull.message());
                    }else{
                        //没有设置可以拼接
                        throw new EntityException(field.getName()+" 不能为空");
                    }
                }
            }
        }
    }
}
