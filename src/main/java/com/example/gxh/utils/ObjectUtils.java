package com.example.gxh.utils;

import java.lang.reflect.Field;

/**
 * @version 1.0
 * @Author gxh
 * @Date 2022/7/27 16:06
 */
public class ObjectUtils {

    /**
     * 判断对象中属性值是否全不为空
     *
     * @param object
     * @return
     */
    public static boolean checkObjAllFieldsIsNull(Object object) {
        if (null == object) {
            return false;
        }
        try {
            for (Field f : object.getClass().getDeclaredFields()) {
                f.setAccessible(true);

                if (f.get(object) == null || f.get(object).toString().equals("") || f.get(object).toString().trim().length() == 0) {
                    return false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
