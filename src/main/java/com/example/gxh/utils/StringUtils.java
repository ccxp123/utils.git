package com.example.gxh.utils;

/**
 * @version 1.0
 * @Author gxh
 * @Date 2022/7/28 15:56
 */
public class StringUtils {

    /**
     * 判断字符不能为空
     *
     * @param strings
     * @return
     */
    public static boolean isNotEmpty(String... strings) {
        for (String str : strings) {
            if (str == null || str.trim().length() == 0)
                return false;
        }
        return true;
    }
}
