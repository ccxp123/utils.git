package com.example.gxh.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author gxh
 * @date 2022-07-28 11:30
 * 自定义注解
 * 判断屬性不为空注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelToEntityNotNull {
    /**
     * 如果字段有该注解，判断为空并返回异常信息
     */
    String message() default "";
}
